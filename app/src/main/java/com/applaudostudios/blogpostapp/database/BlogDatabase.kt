package com.applaudostudios.blogpostapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.applaudostudios.blogpostapp.dao.CommentDao
import com.applaudostudios.blogpostapp.dao.PostDao
import com.applaudostudios.blogpostapp.dao.UserDao
import com.applaudostudios.blogpostapp.model.Comment
import com.applaudostudios.blogpostapp.model.Post
import com.applaudostudios.blogpostapp.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [User::class, Post::class, Comment::class], version = 1, exportSchema = false)
abstract class BlogDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao

    private class BlogDatabaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let {
                scope.launch {
                    val commentDao = it.commentDao()
                    commentDao.deleteAll()
                    val postDao = it.postDao()
                    postDao.deleteAll()
                    val userDao = it.userDao()
                    userDao.deleteAll()
                }
            }
        }

    }

    companion object {

        @Volatile
        private var INSTANCE: BlogDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): BlogDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BlogDatabase::class.java,
                    "blog_database"
                )
                    .addCallback(BlogDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }

        fun getDatabase(context: Context): BlogDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BlogDatabase::class.java,
                    "blog_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }

    }

}