package com.applaudostudios.blogpostapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.applaudostudios.blogpostapp.repository.CommentsRepository
import com.applaudostudios.blogpostapp.repository.PostsRepository

class PostsCommentsViewModelFactory(
    private val commentRepository: CommentsRepository,
    private val postsRepository: PostsRepository,
    private val postId: Int
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostsCommentsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PostsCommentsViewModel(commentRepository, postsRepository, postId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}