package com.applaudostudios.blogpostapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.applaudostudios.blogpostapp.relationship.PostWithComments
import com.applaudostudios.blogpostapp.repository.CommentsRepository
import com.applaudostudios.blogpostapp.repository.PostsRepository
import kotlinx.coroutines.launch

class PostsCommentsViewModel(
    private val commentRepository: CommentsRepository,
    postsRepository: PostsRepository,
    private val postId: Int
) : ViewModel() {

    val allComments: LiveData<PostWithComments> =
        postsRepository.getPostComments(postId).asLiveData()

    fun insertComments() = viewModelScope.launch {
        commentRepository.refreshComments(postId)
    }

}