package com.applaudostudios.blogpostapp.viewmodel

import androidx.lifecycle.*
import com.applaudostudios.blogpostapp.model.Post
import com.applaudostudios.blogpostapp.repository.PostsRepository
import kotlinx.coroutines.launch

class PostsViewModel(
    private val postsRepository: PostsRepository
) : ViewModel() {

    val allPosts: LiveData<List<Post>> = postsRepository.allPost.asLiveData()
    val refreshSucceeded = MutableLiveData<Boolean>()

    /*init {
        refreshSucceeded.value = true
    }*/

    fun insertPosts() = viewModelScope.launch {
        postsRepository.refreshPosts()
        refreshSucceeded.value = postsRepository.refreshSucceeded
    }

}