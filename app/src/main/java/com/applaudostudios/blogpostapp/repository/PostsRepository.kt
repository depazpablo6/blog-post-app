package com.applaudostudios.blogpostapp.repository

import com.applaudostudios.blogpostapp.dao.PostDao
import com.applaudostudios.blogpostapp.dao.UserDao
import com.applaudostudios.blogpostapp.model.Post
import com.applaudostudios.blogpostapp.webservice.PostService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class PostsRepository(private val postDao: PostDao, private val userDao: UserDao) {

    val allPost: Flow<List<Post>> = postDao.getPostsMainInfo()
    var refreshSucceeded: Boolean = false

    fun getPostComments(postId: Int) = postDao.getPostWithComments(postId)

    suspend fun refreshPosts() {
        withContext(Dispatchers.IO) {

            val posts = PostService.getAllPost()

            posts?.let {
                val users = PostService.getUsers(it)
                userDao.deleteAll()
                userDao.insertAll(users)
                postDao.insertAll(it)
                refreshSucceeded = true
            } ?: run { refreshSucceeded = false }

        }
    }


}