package com.applaudostudios.blogpostapp.repository

import com.applaudostudios.blogpostapp.dao.CommentDao
import com.applaudostudios.blogpostapp.webservice.PostService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CommentsRepository(private val commentDao: CommentDao) {

    suspend fun refreshComments(postId: Int) {
        withContext(Dispatchers.IO) {

            val comments = PostService.getPostComment(postId)

            comments?.let {
                commentDao.insertAll(it)
            }

        }
    }

}