package com.applaudostudios.blogpostapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudostudios.blogpostapp.adapter.PostsAdapter
import com.applaudostudios.blogpostapp.app.PostApplication
import com.applaudostudios.blogpostapp.databinding.ActivityMainBinding
import com.applaudostudios.blogpostapp.viewmodel.PostsViewModel
import com.applaudostudios.blogpostapp.viewmodel.PostsViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var postAdapter: PostsAdapter

    private val postsViewModel: PostsViewModel by viewModels {
        PostsViewModelFactory((application as PostApplication).postsRepository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        postsViewModel.insertPosts()
        createActivity()
        val postRecycler = binding.postRecycler
        postRecycler.adapter = postAdapter
        postRecycler.layoutManager = LinearLayoutManager(this)
        postsViewModel.allPosts.observe(this, {
            it?.let {
                postAdapter.submitList(it)
            }
        })

        postsViewModel.refreshSucceeded.observe(this, {
            it?.let {
                if (!it) {
                    Toast.makeText(
                        applicationContext,
                        "Error while retrieving data, make sure you have Internet Connection",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            binding.refreshPostsLayout.isRefreshing = false
        })

        binding.refreshPostsLayout.setOnRefreshListener {
            postsViewModel.insertPosts()
        }

    }

    private fun createActivity() {
        postAdapter = PostsAdapter {
            val intent = Intent(this, PostCommentsActivity::class.java).apply {
                putExtra(POST_ID, it)
            }
            startActivity(intent)
        }
    }

    companion object {
        private const val POST_ID = "postId"
    }


}