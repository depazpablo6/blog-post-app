package com.applaudostudios.blogpostapp.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.applaudostudios.blogpostapp.model.Comment
import com.applaudostudios.blogpostapp.model.Post

data class PostWithComments(
    @Embedded val post: Post,
    @Relation(
        parentColumn = "postId",
        entityColumn = "commentPostId"
    )
    val comments: List<Comment>?
)