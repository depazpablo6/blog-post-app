package com.applaudostudios.blogpostapp

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudostudios.blogpostapp.adapter.CommentsAdapter
import com.applaudostudios.blogpostapp.app.PostApplication
import com.applaudostudios.blogpostapp.databinding.ActivityPostCommentsBinding
import com.applaudostudios.blogpostapp.viewmodel.PostsCommentsViewModel
import com.applaudostudios.blogpostapp.viewmodel.PostsCommentsViewModelFactory

class PostCommentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPostCommentsBinding

    companion object {
        const val POST_ID = "postId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPostCommentsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val postId = intent.getIntExtra(POST_ID, 0)
        val commentsViewModel: PostsCommentsViewModel by viewModels {
            PostsCommentsViewModelFactory(
                (application as PostApplication).commentsRepository,
                (application as PostApplication).postsRepository,
                postId
            )
        }
        commentsViewModel.insertComments()
        val commentsRecycler = binding.commentsRecycler
        val commentsAdapter = CommentsAdapter()
        commentsRecycler.adapter = commentsAdapter
        commentsRecycler.layoutManager = LinearLayoutManager(this)

        commentsViewModel.allComments.observe(this, {
            it?.let {
                binding.postTitleFragment.text = it.post.title
                binding.postUserFragment.text = it.post.ownerUserId.toString()
                binding.postBodyFragment.text = it.post.body
                if (it.comments.isNullOrEmpty()) {
                    commentsViewModel.insertComments()
                } else {
                    commentsAdapter.submitList(it.comments)
                }
            }
        })
    }
}