package com.applaudostudios.blogpostapp.webservice

import com.applaudostudios.blogpostapp.model.Comment
import com.applaudostudios.blogpostapp.model.Post
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PostAPI {

    @GET("/posts")
    fun getAllPost(): Call<List<Post>>

    @GET("/comments?")
    fun getAllPostComments(@Query("postId") postId: Int): Call<List<Comment>>

}