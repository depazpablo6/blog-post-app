package com.applaudostudios.blogpostapp.webservice

import com.applaudostudios.blogpostapp.model.Comment
import com.applaudostudios.blogpostapp.model.Post
import com.applaudostudios.blogpostapp.model.User
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.UnknownHostException

object PostService {

    private const val BASE_URL = "https://jsonplaceholder.typicode.com"

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val postRequests = retrofit.create(PostAPI::class.java)

    fun getAllPost(): List<Post>? {

        var posts: List<Post>? = null

        try {
            posts = postRequests.getAllPost().execute().body()
        } catch (e: UnknownHostException) {
            return posts
        }
        return posts
    }

    fun getPostComment(postId: Int): List<Comment>? {

        var postComment: List<Comment>? = null

        try {
            postComment = postRequests.getAllPostComments(postId).execute().body()
        } catch (e: UnknownHostException) {
            return postComment
        }

        return postComment
    }

    fun getUsers(posts: List<Post>?): List<User> {

        val users: MutableList<User> = ArrayList()

        posts?.let {
            it.forEach { post ->
                val user = User(post.ownerUserId)
                users.add(user)
            }
        }
        return users
    }

}