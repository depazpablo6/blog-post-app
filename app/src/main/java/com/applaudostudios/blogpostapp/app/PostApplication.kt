package com.applaudostudios.blogpostapp.app

import android.app.Application
import androidx.work.*
import com.applaudostudios.blogpostapp.database.BlogDatabase
import com.applaudostudios.blogpostapp.repository.CommentsRepository
import com.applaudostudios.blogpostapp.repository.PostsRepository
import com.applaudostudios.blogpostapp.worker.RefreshBlogDataWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class PostApplication : Application() {

    private val applicationScope = CoroutineScope(SupervisorJob())
    private val database by lazy { BlogDatabase.getDatabase(this, applicationScope) }
    val postsRepository by lazy { PostsRepository(database.postDao(), database.userDao()) }
    val commentsRepository by lazy { CommentsRepository(database.commentDao()) }

    private fun refreshingData() {

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val refreshRequest = PeriodicWorkRequestBuilder<RefreshBlogDataWorker>(15, TimeUnit.MINUTES)
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            RefreshBlogDataWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            refreshRequest
        )

    }

    private fun delayedInit() {
        applicationScope.launch {
            refreshingData()
        }
    }

    override fun onCreate() {
        super.onCreate()
        delayedInit()
    }

}