package com.applaudostudios.blogpostapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.blogpostapp.R
import com.applaudostudios.blogpostapp.model.Post

class PostsAdapter(private val onClickItem: (Int) -> Unit) :
    ListAdapter<Post, PostsAdapter.PostViewHolder>(PostsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(getItem(position), onClickItem)
    }

    class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val postTitle: TextView = itemView.findViewById(R.id.postTitle)
        private val postBody: TextView = itemView.findViewById(R.id.postBody)
        private val postUser: TextView = itemView.findViewById(R.id.postUser)


        fun bind(post: Post, clickItem: (Int) -> Unit) {
            postTitle.text = post.title
            postBody.text = post.body
            postUser.text = post.ownerUserId.toString()
            itemView.setOnClickListener {
                clickItem(post.postId)
            }
        }

        companion object {
            fun create(parent: ViewGroup): PostViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return PostViewHolder(view)
            }
        }
    }

    class PostsComparator : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
            return (oldItem.title == newItem.title &&
                    oldItem.body == newItem.body &&
                    oldItem.ownerUserId == newItem.ownerUserId &&
                    oldItem.postId == newItem.postId)
        }

    }

}