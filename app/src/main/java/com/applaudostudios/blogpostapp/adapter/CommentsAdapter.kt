package com.applaudostudios.blogpostapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.blogpostapp.R
import com.applaudostudios.blogpostapp.model.Comment

class CommentsAdapter :
    ListAdapter<Comment, CommentsAdapter.CommentViewHolder>(CommentsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.body, current.name, current.email)
    }


    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val commentBody: TextView = itemView.findViewById(R.id.commentBody)
        private val commentUser: TextView = itemView.findViewById(R.id.fromUser)
        private val commentName: TextView = itemView.findViewById(R.id.commentName)

        fun bind(body: String?, name: String?, email: String?) {
            commentBody.text = body
            val fromUser = "From $email"
            commentUser.text = fromUser
            commentName.text = name
        }

        companion object {
            fun create(parent: ViewGroup): CommentViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_comment_item, parent, false)
                return CommentViewHolder(view)
            }
        }
    }

    class CommentsComparator : DiffUtil.ItemCallback<Comment>() {
        override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
            return (oldItem.body == newItem.body &&
                    oldItem.commentId == newItem.commentId &&
                    oldItem.commentPostId == newItem.commentPostId &&
                    oldItem.email == newItem.email &&
                    oldItem.name == newItem.name)
        }

    }

}