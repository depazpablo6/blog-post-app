package com.applaudostudios.blogpostapp.dao

import androidx.room.*
import com.applaudostudios.blogpostapp.model.Post
import com.applaudostudios.blogpostapp.relationship.PostWithComments
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(posts: List<Post>)

    @Query("SELECT postId, title, body, ownerUserId FROM Post")
    fun getPostsMainInfo(): Flow<List<Post>>

    @Query("DELETE FROM Post")
    fun deleteAll()

    @Transaction
    @Query("SELECT * FROM Post WHERE postId = :postId")
    fun getPostWithComments(postId: Int): Flow<PostWithComments>

}