package com.applaudostudios.blogpostapp.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.blogpostapp.model.Comment

@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(comments: List<Comment>)

    @Query("DELETE FROM Comment")
    fun deleteAll()

}