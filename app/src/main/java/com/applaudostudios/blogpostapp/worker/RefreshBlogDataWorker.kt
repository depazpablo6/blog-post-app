package com.applaudostudios.blogpostapp.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.applaudostudios.blogpostapp.database.BlogDatabase.Companion.getDatabase
import com.applaudostudios.blogpostapp.repository.PostsRepository

class RefreshBlogDataWorker(context: Context, parameters: WorkerParameters) :
    CoroutineWorker(context, parameters) {

    companion object {
        const val WORK_NAME = "com.applaudostudios.blogpostapp.worker.RefreshBlogDataWorker.kt"
    }

    override suspend fun doWork(): Result {
        val database = getDatabase(applicationContext)
        val postsRepository = PostsRepository(database.postDao(), database.userDao())

        return try {
            postsRepository.refreshPosts()
            Result.success()
        } catch (e: Exception) {
            Result.retry()
        }

    }
}